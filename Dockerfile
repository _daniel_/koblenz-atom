FROM ubuntu:bionic
WORKDIR /app
COPY . .
RUN apt-get update
RUN apt-get install -y cargo pkg-config libssl-dev
RUN cargo build --release
