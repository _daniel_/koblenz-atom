use cached_path::Cache;
use users::get_current_username;
use std::{fs, time::SystemTime};
use reqwest::blocking::Client;

pub struct Http {
    cache: Option<Cache>,
    client: Client,

}

impl Http {
    pub fn new(use_cache: bool) -> Http {
        if use_cache {
            let cache = Cache::builder()
                .dir(std::env::temp_dir().join(format!("{}_koblenz-atom", username())))
                .build()
                .unwrap();
            return Http{
                client: Client::new(),
                cache: Some(cache)
            };
        }
        return Http {
            client: Client::new(),
            cache: None
        };
    }

    pub fn get(&self, url: &str) -> (String, SystemTime) {
        match &self.cache {
            Some(cache) => {
                let path = cache.cached_path(url).unwrap();
                let contents = fs::read_to_string(&path).expect("could not read cached data");
                let modified = fs::metadata(&path).unwrap().modified().unwrap();
                (contents, modified)
            }
            None => {
                let response = self.client.get(url).send().unwrap();
                // TODO: can i get last_modified from the header und use this as modified time?
                (response.text().unwrap(), SystemTime::now())
            }
        }
    }
}

fn username() -> String {
    match get_current_username() {
        Some(s) => s.into_string().unwrap_or("".to_string()),
        _ => "".to_string(),
    }
}
