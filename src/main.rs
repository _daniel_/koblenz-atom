use koblenz_atom::Http;
use atom_syndication::{Content, Entry, Feed, FixedDateTime, Link, Person};
use chrono::{DateTime, FixedOffset, Local, NaiveDate};
use html_escape::encode_text;
use scraper::{ElementRef, Html, Selector};
use url::Url;

fn main() {
    let http = Http::new(false);

    let mut feed = Feed::default();
    feed.set_title("Pressemeldungen der Stadt Koblenz");
    // url to announcements
    let koblenz = "https://www.koblenz.de/rathaus/verwaltung/pressemeldungen/";
    feed.set_id(koblenz);

    let (contents, modified) = http.get(&koblenz);
    feed.set_updated(DateTime::<Local>::from(modified));


    let document = Html::parse_document(&contents);

    let pagination = get_pagination(&document);
    let pages = vec![document].into_iter().chain(
        pagination
            .iter()
            .map(|page_url| {
                Html::parse_document(
                    &http.get(page_url).0
                )
            })
            .into_iter(),
    );

    // crash, when selector for articles cannot be generated
    let article_selector = Selector::parse(r#"article"#).unwrap();

    let entries: Vec<Entry> = pages
        .map(|page| {
            page.select(&article_selector)
                .filter_map(|article| extract_entry(article, &http))
                .collect::<Vec<Entry>>()
        })
        .flatten()
        .collect();

    // set updated to most recent datetime
    feed.set_updated(get_most_recent_udpated(&entries, &feed));

    feed.set_entries(entries);
    println!("{}", feed.to_string());
}

fn get_pagination(document: &Html) -> Vec<String> {
    let urlstrings: Vec<String> = document
        .select(&Selector::parse(r#"ul[class="pagination"] li a"#).unwrap())
        .map(|a| build_url(a.value().attr("href").unwrap()))
        .collect();
    let urls = urlstrings
        .iter()
        .map(|u| Url::parse(u).unwrap())
        .collect::<Vec<Url>>();

    let pagenums = 2..=urls
        .iter()
        .map(|u| {
            u.query_pairs()
                .filter(|(k, _)| k.to_string() == "page")
                .next()
                .unwrap()
                .1
                .parse::<i32>()
                .unwrap()
        })
        .max()
        .unwrap();
    let paged_block_id = urls
        .first()
        .unwrap()
        .query_pairs()
        .filter(|(k, _)| k.to_string().to_lowercase() == "pagedblockid")
        .next()
        .unwrap()
        .1
        .to_string();
    let first_url: &Url = urls.first().unwrap();
    pagenums
        .into_iter()
        .map(|i| {
            format!(
                "{}?pagedBlockID={}&page={}",
                build_url(first_url.path()),
                paged_block_id,
                i
            )
        })
        .collect::<Vec<String>>()
}

fn get_most_recent_udpated(entries: &Vec<Entry>, feed: &Feed) -> DateTime<FixedOffset> {
    entries
        .iter()
        .map(|e| e.updated().clone())
        .chain(vec![feed.updated].into_iter())
        .max_by(|x, y| x.timestamp().cmp(&y.timestamp()))
        .unwrap()
        .clone()
}

fn extract_entry(article: ElementRef, http: &Http) -> Option<Entry> {
    let url = extract_url(&article)?;
    let mut link = Link::default();
    link.set_href(&url);
    let mut entry = Entry::default();
    entry.set_title(&extract_title(article)?);
    entry.set_links(vec![link]);
    entry.set_id(&url);

    let (article_contents, article_modified) = http.get(&url);
    entry.set_updated(DateTime::<Local>::from(
        article_modified
    ));

    let article_document = Html::parse_document(&article_contents);
    entry.set_published(extract_article_published(&article_document)?);

    let mut author = Person::default();
    author.set_name("Stadt Koblenz");
    entry.set_authors(vec![author]);

    entry.set_content(extract_article_content(&article_document));
    Some(entry)
}

fn extract_title(article: ElementRef) -> Option<String> {
    Some(
        article
            .select(&Selector::parse(r#"[class="headline"]"#).ok()?)
            .next()?
            .inner_html(),
    )
}
fn extract_url(article: &ElementRef) -> Option<String> {
    Some(build_url(
        article
            .select(&Selector::parse(r#"a"#).ok()?)
            .next()?
            .value()
            .attr("href")?,
    ))
}
fn extract_article_published(document: &Html) -> Option<FixedDateTime> {
    let published = document
        .select(&Selector::parse(r#"time"#).ok()?)
        .next()?
        .inner_html();
    Some(FixedDateTime::from_utc(
        NaiveDate::parse_from_str(
            &published,
            if published.contains(".") {
                "%d.%m.%Y"
            } else {
                "%d/%m/%Y"
            },
        )
        .ok()?
        .and_hms(0, 0, 0),
        FixedOffset::west(0),
    ))
}
fn extract_article_content(document: &Html) -> Option<Content> {
    let text = document
        .select(&Selector::parse(r#"div[class="press-release__text"]"#).ok()?)
        .next()?
        .inner_html();
    let escaped_text = encode_text(&text).to_string();
    let mut content = Content::default();
    content.set_value(escaped_text);
    content.set_content_type("html".to_string());
    Some(content)
}

fn build_url(start: &str) -> String {
    if !start.starts_with("https://") && !start.starts_with("http://") {
        let mut p: String = "https://www.koblenz.de".to_owned();
        p.push_str(start);
        return p;
    }
    start.to_owned()
}

